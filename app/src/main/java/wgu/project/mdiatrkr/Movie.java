package wgu.project.mdiatrkr;

public class Movie extends Disc{
    private String title;
    private int duration;   // in minutes
    private String genre;   // of movie contained
    private String rating;  // R, PG, G, etc.

    //Constructor with initialized default values
    public Movie() {
        super("DVD","false");
        this.title = "";
        this.duration = 0;
        this.genre = "";
        this.rating = "";
    }

    //Getters and Setters
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}
