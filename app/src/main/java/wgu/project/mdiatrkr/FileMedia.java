package wgu.project.mdiatrkr;

public class FileMedia extends Disc {
    private String discOrigin;   // Retail or homemade
    private String contents;    // quick summary of contents

    //Constructor with initialized default values
    public FileMedia() {
        super("DVD","false");;
        this.discOrigin = "";
        this.contents = "";
    }

    //Getters and Setters
    public String getDiscOrigin() {
        return discOrigin;
    }

    public void setDiscOrigin(String discType) {
        this.discOrigin = discType;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }
}
