package wgu.project.mdiatrkr;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


public class DBProvider extends ContentProvider {

    // Path strings
    private static final String AUTHORITY = "wgu.project.mdiatrkr.DBProvider";
    private static final String MOVIES_PATH = "movies";
    private static final String MEDIA_PATH = "media";
    private static final String DISCS_PATH = "discs";

    // URIs generate from path strings
    public static final Uri MOVIES_URI = Uri.parse("content://"+ AUTHORITY + "/"+ MOVIES_PATH);
    public static final Uri MEDIA_URI = Uri.parse("content://"+ AUTHORITY + "/"+ MEDIA_PATH);
    public static final Uri DISCS_URI = Uri.parse("content://"+ AUTHORITY + "/"+ DISCS_PATH);

    // Operation identifier constants
    private static final int MOVIES = 1;
    private static final int MEDIA = 2;
    private static final int DISCS = 3;

    // URI Match method
    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        uriMatcher.addURI(AUTHORITY, MOVIES_PATH, MOVIES);
        uriMatcher.addURI(AUTHORITY, MEDIA_PATH, MEDIA);
        uriMatcher.addURI(AUTHORITY, DISCS_PATH, DISCS);
    }
    private SQLiteDatabase db;

    @Override
    public boolean onCreate() {
        DBHelper helper = new DBHelper(getContext());
        db = helper.getWritableDatabase();
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        switch (uriMatcher.match(uri)){
            case MOVIES:
                return db.query(DBHelper.TABLE_MOVIES, DBHelper.MOVIES_COLUMNS, selection, null,
                        null,null,DBHelper.MOVIES_ID + " ASC");
            case MEDIA:
                return db.query(DBHelper.TABLE_MEDIA, DBHelper.MEDIA_COLUMNS, selection, null,
                        null,null,DBHelper.MEDIA_ID + " ASC");
            case DISCS:
                return db.query(DBHelper.VIEW_DISCS, DBHelper.DISCS_COLUMNS, selection, null,
                        null,null,DBHelper.DISCS_CONTENTS + " ASC");
            default:
                throw new IllegalArgumentException("Invalid URI:"+ uri);
        }

    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        long id;
        switch (uriMatcher.match(uri)){
            case MOVIES:
                id = db.insert(DBHelper.TABLE_MOVIES, null, values);
                return uri.parse(MOVIES_PATH + "/" + id);
            case MEDIA:
                id = db.insert(DBHelper.TABLE_MEDIA, null, values);
                return uri.parse(MEDIA_PATH + "/" + id);
            default:
                throw new IllegalArgumentException("Invalid URI:"+ uri);
        }
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        switch (uriMatcher.match(uri)){
            case MOVIES:
                return db.delete(DBHelper.TABLE_MOVIES, selection, selectionArgs);
            case MEDIA:
                return db.delete(DBHelper.TABLE_MEDIA, selection, selectionArgs);
            default:
                throw new IllegalArgumentException("Invalid URI:"+ uri);
        }
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        switch (uriMatcher.match(uri)){
            case MOVIES:
                return db.update(DBHelper.TABLE_MOVIES, values, selection, selectionArgs);
            case MEDIA:
                return db.update(DBHelper.TABLE_MEDIA, values, selection, selectionArgs);
            default:
                throw new IllegalArgumentException("Invalid URI:"+ uri);
        }
    }
}



