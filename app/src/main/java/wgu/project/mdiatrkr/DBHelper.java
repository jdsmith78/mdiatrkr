package wgu.project.mdiatrkr;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "mdiatrkr.db";
    public static final int DATABASE_VERSION = 1;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Constant strings for Movies table
    public static final String TABLE_MOVIES = "tbl_movies";
    public static final String MOVIES_ID = "_id";
    public static final String MOVIES_FORMAT = "format";
    public static final String MOVIES_REWRITABLE = "rewritable";
    public static final String MOVIES_TITLE = "title";
    public static final String MOVIES_DURATION = "duration";
    public static final String MOVIES_GENRE = "genre";
    public static final String MOVIES_RATING = "rating";
    public static final String[] MOVIES_COLUMNS = {MOVIES_ID,MOVIES_FORMAT,MOVIES_REWRITABLE,MOVIES_TITLE,MOVIES_DURATION,MOVIES_GENRE,MOVIES_RATING};

    // Constant strings for Media table
    public static final String TABLE_MEDIA = "tbl_media";
    public static final String MEDIA_ID = "_id";
    public static final String MEDIA_FORMAT = "format";
    public static final String MEDIA_REWRITABLE = "rewritable";
    public static final String MEDIA_DISC_ORIGIN = "disc_type";
    public static final String MEDIA_CONTENTS = "contents";
    public static final String[] MEDIA_COLUMNS = {MEDIA_ID,MEDIA_FORMAT,MEDIA_REWRITABLE,MEDIA_DISC_ORIGIN,MEDIA_CONTENTS};

    // Constant strings for Discs view
    public static final String VIEW_DISCS = "view_discs";
    public static final String DISCS_ID = "_id";
    public static final String DISCS_FORMAT = "format";
    public static final String DISCS_REWRITABLE = "rewritable";
    public static final String DISCS_CONTENTS = "contents";
    public static final String[] DISCS_COLUMNS = {DISCS_ID,DISCS_FORMAT,DISCS_REWRITABLE,DISCS_CONTENTS};

    // Creation statements to make working with the DB easier throughout the application broken down by table

    // SQL to create Movies table
    private static final String CREATE_MOVIES_TABLE =
            "CREATE TABLE " + TABLE_MOVIES + " (" +
                    MOVIES_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    MOVIES_FORMAT + " TEXT, " +
                    MOVIES_REWRITABLE + " TEXT," +
                    MOVIES_TITLE + " TEXT, " +
                    MOVIES_DURATION + " INTEGER," +
                    MOVIES_GENRE + " TEXT," +
                    MOVIES_RATING + " TEXT" +
                    ")";

    // SQL to create FileMedia table
    private static final String CREATE_MEDIA_TABLE =
            "CREATE TABLE " + TABLE_MEDIA + " (" +
                    MEDIA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    MEDIA_FORMAT + " TEXT," +
                    MEDIA_REWRITABLE + " TEXT," +
                    MEDIA_DISC_ORIGIN + " TEXT, " +
                    MEDIA_CONTENTS + " TEXT" +
                    ")";

    // SQL to create Discs view for reports
    private static final String CREATE_DISCS_VIEW =
            "CREATE VIEW " + VIEW_DISCS + " AS "+
                "SELECT "+  MOVIES_ID + ", "+
                            MOVIES_FORMAT + ", "+
                            MOVIES_REWRITABLE + ", "+
                            MOVIES_TITLE + " AS "+ MEDIA_CONTENTS +" "+
                "FROM "+TABLE_MOVIES+" "+
                "UNION "+
                "SELECT "+  MEDIA_ID + ", "+
                            MEDIA_FORMAT + ", "+
                            MEDIA_REWRITABLE + ", "+
                            MEDIA_CONTENTS + " "+
                "FROM "+TABLE_MEDIA;


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_MOVIES_TABLE);
        db.execSQL(CREATE_MEDIA_TABLE);
        db.execSQL(CREATE_DISCS_VIEW);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MOVIES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MEDIA);
        db.execSQL("DROP VIEW IF EXISTS " + VIEW_DISCS);
        onCreate(db);
    }

}
