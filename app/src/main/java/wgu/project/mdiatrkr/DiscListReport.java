package wgu.project.mdiatrkr;


import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;

import com.example.mdiatrkr.R;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class DiscListReport extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>{

    private CursorAdapter mediaCursorAdapter;
    private DBProvider db;
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disc_list_report);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //set timestamp field value
        TextView dateTimestampText = findViewById(R.id.disc_report_timestamp);
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        dateTimestampText.setText(sdf.format(timestamp));

        //Display existing discs
        String[] fromColumns = {DBHelper.DISCS_FORMAT, DBHelper.DISCS_CONTENTS};
        int [] toViews = {R.id.report_item_format, R.id.report_item_content};

        // Create cursor
        mediaCursorAdapter = new SimpleCursorAdapter(this,R.layout.disc_list_item,null, fromColumns, toViews,0);
        db = new DBProvider();

        ListView listView = findViewById(R.id.disc_list_view);
        listView.setAdapter(mediaCursorAdapter);

        getSupportLoaderManager().initLoader(0,null,this);
    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int i, @Nullable Bundle bundle) {
        //Consider modifying order of display
        return new CursorLoader(this, DBProvider.DISCS_URI, null,null,null,null);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor cursor){
        mediaCursorAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(@NonNull Loader loader) {
        mediaCursorAdapter.swapCursor(null);
    }

    @Override
    public void onResume(){
        super.onResume();
        getSupportLoaderManager().restartLoader(0,null,this);
    }


}