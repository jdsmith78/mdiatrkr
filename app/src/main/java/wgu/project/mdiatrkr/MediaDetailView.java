package wgu.project.mdiatrkr;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.mdiatrkr.R;

public class MediaDetailView extends AppCompatActivity {
    long mediaId;
    FileMedia media;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_detail);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        mediaId = intent.getLongExtra("mediaId",-1);

        // Query db for movie requested
        Cursor tempCursor = getContentResolver().query(DBProvider.MEDIA_URI,null,DBHelper.MEDIA_ID+" = "+mediaId,null,null);
        if (tempCursor.moveToFirst()){
            media = convertCursorToFileMedia(tempCursor);
        }

        //Identify objects from layout for movie text input
        TextView mediaFormat = findViewById(R.id.media_detail_format);
        TextView mediaContents = findViewById(R.id.media_detail_contents);
        TextView mediaDiscOrigin = findViewById(R.id.media_detail_origin);
        TextView mediaRewritable = findViewById(R.id.media_detail_rewriteable);

        //Set values fetched from db
        mediaFormat.setText(media.getFormat());
        mediaContents.setText(media.getContents());
        mediaDiscOrigin.setText(media.getDiscOrigin());
        mediaRewritable.setText(media.getRewritable());

        //Set up button functionality
        Button editMediaButton = findViewById(R.id.media_detail_edit_media);
        editMediaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaDetailView.this,DiscInput.class);
                intent.putExtra("discType","Files or Media");
                intent.putExtra("discId",mediaId);
                startActivity(intent);
            }
        });

        Button deleteMediaButton = findViewById(R.id.media_detail_delete_media);
        deleteMediaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int delItem = getContentResolver().delete(DBProvider.MEDIA_URI,DBHelper.MEDIA_ID+" = "+mediaId,null);
                finish();
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        this.recreate();
    }

    public static FileMedia convertCursorToFileMedia(Cursor cursor){
        FileMedia fetchedMedia = new FileMedia();
        fetchedMedia.setContents(cursor.getString(cursor.getColumnIndex(DBHelper.MEDIA_CONTENTS)));
        fetchedMedia.setDiscOrigin(cursor.getString(cursor.getColumnIndex(DBHelper.MEDIA_DISC_ORIGIN)));
        fetchedMedia.setFormat(cursor.getString(cursor.getColumnIndex(DBHelper.MEDIA_FORMAT)));
        fetchedMedia.setRewritable(cursor.getString(cursor.getColumnIndex(DBHelper.MEDIA_REWRITABLE)));
        return fetchedMedia;
    }
}