package wgu.project.mdiatrkr;


import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.example.mdiatrkr.R;

public class MovieListView extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>{

    private CursorAdapter cursorAdapter;
    private DBProvider db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_view);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Display existing movies
        String[] fromColumns = {DBHelper.MOVIES_TITLE, DBHelper.MOVIES_GENRE, DBHelper.MOVIES_DURATION};
        int [] toViews = {R.id.movie_item_title,R.id.movie_item_genre, R.id.movie_item_duration};

        // Create cursor
        cursorAdapter = new SimpleCursorAdapter(this,R.layout.movie_list_item,null, fromColumns, toViews,0);
        db = new DBProvider();

        ListView listView = findViewById(R.id.movie_list_view);
        listView.setAdapter(cursorAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long movieId) {
                Intent intent = new Intent(MovieListView.this, MovieDetailView.class);
                intent.putExtra("movieId",movieId);
                startActivity(intent);
            }
        });

        getSupportLoaderManager().initLoader(0,null,this);

        Button addMovieButton = findViewById(R.id.add_movie_button);
        addMovieButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MovieListView.this, DiscInput.class);
                intent.putExtra("discType","Movie");
                startActivity(intent);
            }
        });
    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int i, @Nullable Bundle bundle) {
        //Consider modifying order of display
        return new CursorLoader(this, DBProvider.MOVIES_URI, null,null,null,null);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor cursor){
        cursorAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(@NonNull Loader loader) {
        cursorAdapter.swapCursor(null);
    }

    @Override
    public void onResume(){
        super.onResume();
        getSupportLoaderManager().restartLoader(0,null,this);
    }


}