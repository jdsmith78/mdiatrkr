package wgu.project.mdiatrkr;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;

import com.example.mdiatrkr.R;


public class DiscSearch extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>{

    private CursorAdapter cursorAdapter;
    private DBProvider db;
    private EditText searchTermText;
    private String searchTerm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disc_search);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Create cursor
        String[] fromColumns = {DBHelper.DISCS_FORMAT, DBHelper.DISCS_CONTENTS};
        int [] toViews = {R.id.search_result_format,R.id.search_result_content};

        cursorAdapter = new SimpleCursorAdapter(this,R.layout.search_result_item,null, fromColumns, toViews,0);
        db = new DBProvider();

        final ListView resultListView = findViewById(R.id.disc_search_result_list);
        resultListView.setAdapter(cursorAdapter);
        resultListView.setVisibility(View.GONE);

        // Layout setup
        searchTermText = findViewById(R.id.disc_search_term_field);

        //Search Button
        Button searchButton = findViewById(R.id.disc_search_button);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (searchTermText.getText().toString().equals("")){
                    Toast.makeText(DiscSearch.this,"An empty search field returns all discs.",Toast.LENGTH_LONG).show();
                    }
                searchTerm = searchTermText.getText().toString();
                resultListView.setVisibility(View.VISIBLE);
                getSupportLoaderManager().restartLoader(0,null,DiscSearch.this);
            }
        });
        }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int i, @Nullable Bundle bundle) {
        return new CursorLoader(this, DBProvider.DISCS_URI, null,DBHelper.DISCS_CONTENTS+" LIKE '%"+searchTerm+"%'",null,null);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor cursor){
        cursorAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(@NonNull Loader loader) {
        cursorAdapter.swapCursor(null);
    }

    @Override
    public void onResume(){
        super.onResume();
        getSupportLoaderManager().restartLoader(0,null,this);
    }
}