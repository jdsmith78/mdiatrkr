package wgu.project.mdiatrkr;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import com.example.mdiatrkr.R;

public class MainActivity extends AppCompatActivity {
    DBHelper myHelper;
    private SQLiteDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_screen);

        myHelper = new DBHelper(MainActivity.this);
        database = myHelper.getWritableDatabase();
//       Toast.makeText(MainActivity.this,myHelper.getDatabaseName(),1).show();

        Toolbar toolbar = findViewById(R.id.app_main_toolbar);
        setSupportActionBar(toolbar);
        Button viewMoviesButton = findViewById(R.id.button_view_movies);
        viewMoviesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MovieListView.class);
                startActivity(intent);
            }
        });

        Button viewMediaButton = findViewById(R.id.button_view_media);
        viewMediaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MediaListView.class);
                intent.putExtra("discType","None");
                startActivity(intent);
            }
        });

        Button addDiscButton = findViewById(R.id.add_disc_button);
        addDiscButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, DiscInput.class);
                intent.putExtra("discType","None");
                startActivity(intent);
            }
        });
        Button searchDiscsButton = findViewById(R.id.button_search_discs);
        searchDiscsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, DiscSearch.class);
                startActivity(intent);
            }
        });

        Button discListReportButton = findViewById(R.id.button_disc_list_report);
        discListReportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, DiscListReport.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_gen_test_data:
                // Generate test data method
                createTestDBEntries(MainActivity.this);
//                Toast.makeText(MainActivity.this,"Test data create placeholder",1).show();
                return true;
            case R.id.menu_clear_db:
                // DB Clear method
                int delTable= getContentResolver().delete(DBProvider.MOVIES_URI,null,null);
                delTable= getContentResolver().delete(DBProvider.MEDIA_URI,null,null);
                return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        database.close();
//        Toast.makeText(MainActivity.this,myHelper.getDatabaseName() +" closed",Toast.LENGTH_SHORT).show();
    }

    private void createTestDBEntries (Context context){
        // First sample movie
        ContentValues cv = new ContentValues();
        cv.put(DBHelper.MOVIES_GENRE,"Adventure");
        cv.put(DBHelper.MOVIES_TITLE,"AutoTestMovie - The Bigfoot of Vases");
        cv.put(DBHelper.MOVIES_REWRITABLE,"No");
        cv.put(DBHelper.MOVIES_FORMAT,"BluRay");
        cv.put(DBHelper.MOVIES_DURATION,123);
        cv.put(DBHelper.MOVIES_RATING,"PG");
        Uri newItem= getContentResolver().insert(DBProvider.MOVIES_URI,cv);
        cv.clear();

        // Second sample movie
        cv.put(DBHelper.MOVIES_GENRE,"Action");
        cv.put(DBHelper.MOVIES_TITLE,"AutoTestMovie - Dye Hard");
        cv.put(DBHelper.MOVIES_REWRITABLE,"No");
        cv.put(DBHelper.MOVIES_FORMAT,"DVD");
        cv.put(DBHelper.MOVIES_DURATION,234);
        cv.put(DBHelper.MOVIES_RATING,"R");
        newItem= getContentResolver().insert(DBProvider.MOVIES_URI,cv);
        cv.clear();

        // Third sample movie
        cv.put(DBHelper.MOVIES_GENRE,"Documentary");
        cv.put(DBHelper.MOVIES_TITLE,"AutoTestMovie - Installing a New Congress");
        cv.put(DBHelper.MOVIES_REWRITABLE,"No");
        cv.put(DBHelper.MOVIES_FORMAT,"BluRay");
        cv.put(DBHelper.MOVIES_DURATION,333);
        cv.put(DBHelper.MOVIES_RATING,"G");
        newItem= getContentResolver().insert(DBProvider.MOVIES_URI,cv);
        cv.clear();

        // Fourth sample movie
        cv.put(DBHelper.MOVIES_GENRE,"Thriller");
        cv.put(DBHelper.MOVIES_TITLE,"AutoTestMovie - Silence of the Hams");
        cv.put(DBHelper.MOVIES_REWRITABLE,"No");
        cv.put(DBHelper.MOVIES_FORMAT,"DVD");
        cv.put(DBHelper.MOVIES_DURATION,444);
        cv.put(DBHelper.MOVIES_RATING,"R");
        newItem= getContentResolver().insert(DBProvider.MOVIES_URI,cv);
        cv.clear();

        // Fifth sample movie
        cv.put(DBHelper.MOVIES_GENRE,"Adventure");
        cv.put(DBHelper.MOVIES_TITLE,"AutoTestMovie - The Brittle Merblade");
        cv.put(DBHelper.MOVIES_REWRITABLE,"No");
        cv.put(DBHelper.MOVIES_FORMAT,"DVD");
        cv.put(DBHelper.MOVIES_DURATION,55);
        cv.put(DBHelper.MOVIES_RATING,"G");
        newItem= getContentResolver().insert(DBProvider.MOVIES_URI,cv);
        cv.clear();

        // Sixth sample movie
        cv.put(DBHelper.MOVIES_GENRE,"Action");
        cv.put(DBHelper.MOVIES_TITLE,"AutoTestMovie - Dye Hard 2 - Dye Harder");
        cv.put(DBHelper.MOVIES_REWRITABLE,"No");
        cv.put(DBHelper.MOVIES_FORMAT,"DVD");
        cv.put(DBHelper.MOVIES_DURATION,66);
        cv.put(DBHelper.MOVIES_RATING,"R");
        newItem= getContentResolver().insert(DBProvider.MOVIES_URI,cv);
        cv.clear();

        // First sample files/media
        cv.put(DBHelper.MEDIA_CONTENTS,"AutoTestMedia - Portals 98");
        cv.put(DBHelper.MEDIA_DISC_ORIGIN,"Retail");
        cv.put(DBHelper.MEDIA_REWRITABLE,"No");
        cv.put(DBHelper.MEDIA_FORMAT,"DVD");
        newItem= getContentResolver().insert(DBProvider.MEDIA_URI,cv);
        cv.clear();

        // Second sample files/media
        cv.put(DBHelper.MEDIA_CONTENTS,"AutoTestMedia - DoorCraft 3");
        cv.put(DBHelper.MEDIA_DISC_ORIGIN,"Homemade");
        cv.put(DBHelper.MEDIA_REWRITABLE,"Yes");
        cv.put(DBHelper.MEDIA_FORMAT,"DVD");
        newItem= getContentResolver().insert(DBProvider.MEDIA_URI,cv);
        cv.clear();

        // Third sample files/media
        cv.put(DBHelper.MEDIA_CONTENTS,"AutoTestMedia - Pictures from Vacation to Pluto");
        cv.put(DBHelper.MEDIA_DISC_ORIGIN,"Retail");
        cv.put(DBHelper.MEDIA_REWRITABLE,"No");
        cv.put(DBHelper.MEDIA_FORMAT,"DVD");
        newItem= getContentResolver().insert(DBProvider.MEDIA_URI,cv);
        cv.clear();

        // Fourth sample files/media
        cv.put(DBHelper.MEDIA_CONTENTS,"AutoTestMedia - Tax document backups");
        cv.put(DBHelper.MEDIA_DISC_ORIGIN,"Homemade");
        cv.put(DBHelper.MEDIA_REWRITABLE,"No");
        cv.put(DBHelper.MEDIA_FORMAT,"CD");
        newItem= getContentResolver().insert(DBProvider.MEDIA_URI,cv);
        cv.clear();

        // Fifth sample files/media
        cv.put(DBHelper.MEDIA_CONTENTS,"AutoTestMedia - Portals 95 Installer");
        cv.put(DBHelper.MEDIA_DISC_ORIGIN,"Homemade");
        cv.put(DBHelper.MEDIA_REWRITABLE,"No");
        cv.put(DBHelper.MEDIA_FORMAT,"CD");
        newItem= getContentResolver().insert(DBProvider.MEDIA_URI,cv);
        cv.clear();

        // Sixth sample files/media
        cv.put(DBHelper.MEDIA_CONTENTS,"AutoTestMedia - DoorCraft 3 - The Molten Commode");
        cv.put(DBHelper.MEDIA_DISC_ORIGIN,"Homemade");
        cv.put(DBHelper.MEDIA_REWRITABLE,"No");
        cv.put(DBHelper.MEDIA_FORMAT,"CD");
        newItem= getContentResolver().insert(DBProvider.MEDIA_URI,cv);
        cv.clear();

        // Seventh sample files/media
        cv.put(DBHelper.MEDIA_CONTENTS,"AutoTestMedia - Tax Returns for Mom");
        cv.put(DBHelper.MEDIA_DISC_ORIGIN,"Homemade");
        cv.put(DBHelper.MEDIA_REWRITABLE,"No");
        cv.put(DBHelper.MEDIA_FORMAT,"CD");
        newItem= getContentResolver().insert(DBProvider.MEDIA_URI,cv);
        cv.clear();

    }
}