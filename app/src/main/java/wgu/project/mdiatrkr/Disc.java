package wgu.project.mdiatrkr;

public class Disc {
    private String format;  //BluRay, DVD, CD
    private String rewritable;

    //Constructor
    public Disc(String format, String rewritable) {
        this.format = format;
        this.rewritable = rewritable;
    }

    //Getters and Setters
    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getRewritable() {return rewritable;
    }

    public void setRewritable(String rewritable) {
        this.rewritable = rewritable;
    }
}
