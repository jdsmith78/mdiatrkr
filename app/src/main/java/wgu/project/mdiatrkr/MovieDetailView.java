package wgu.project.mdiatrkr;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.mdiatrkr.R;

public class MovieDetailView extends AppCompatActivity {
    long movieId;
    Movie movie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        movieId = intent.getLongExtra("movieId",-1);

        // Query db for movie requested
        Cursor tempCursor = getContentResolver().query(DBProvider.MOVIES_URI,null,DBHelper.MOVIES_ID+" = "+movieId,null,null);
        if (tempCursor.moveToFirst()){
            movie = convertCursorToMovie(tempCursor);
        }

        //Identify objects from layout for movie text input
        TextView movieTitle = findViewById(R.id.movie_detail_title);
        TextView movieGenre = findViewById(R.id.movie_detail_genre);
        TextView movieRating = findViewById(R.id.movie_detail_rating);
        TextView movieDuration= findViewById(R.id.movie_detail_duration);
        TextView movieFormat= findViewById(R.id.movie_detail_format);
        TextView movieRewriteable= findViewById(R.id.movie_detail_rewriteable);

        //Set values fetched from db
        movieTitle.setText(movie.getTitle());
        movieGenre.setText(movie.getGenre());
        movieRating.setText(movie.getRating());
        movieDuration.setText((String.valueOf(movie.getDuration())));
        movieFormat.setText(movie.getFormat());
        movieRewriteable.setText(movie.getRewritable());

        //Set up button functionality
        Button editMovieButton = findViewById(R.id.movie_detail_edit_movie);
        editMovieButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MovieDetailView.this,DiscInput.class);
                intent.putExtra("discType","Movie");
                intent.putExtra("discId",movieId);
                startActivity(intent);
            }
        });

        Button deleteMovieButton = findViewById(R.id.movie_detail_delete_movie);
        deleteMovieButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int delItem = getContentResolver().delete(DBProvider.MOVIES_URI,DBHelper.MOVIES_ID+" = "+movieId,null);
                finish();
                return;
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        this.recreate();
    }

    public static Movie convertCursorToMovie(Cursor cursor){
        Movie fetchedMovie = new Movie();
        fetchedMovie.setTitle(cursor.getString(cursor.getColumnIndex(DBHelper.MOVIES_TITLE)));
        fetchedMovie.setDuration(cursor.getInt(cursor.getColumnIndex(DBHelper.MOVIES_DURATION)));
        fetchedMovie.setGenre(cursor.getString(cursor.getColumnIndex(DBHelper.MOVIES_GENRE)));
        fetchedMovie.setRating(cursor.getString(cursor.getColumnIndex(DBHelper.MOVIES_RATING)));
        fetchedMovie.setFormat(cursor.getString(cursor.getColumnIndex(DBHelper.MOVIES_FORMAT)));
        fetchedMovie.setRewritable(cursor.getString(cursor.getColumnIndex(DBHelper.MOVIES_REWRITABLE)));
        return fetchedMovie;
    }
}