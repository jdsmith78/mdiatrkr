package wgu.project.mdiatrkr;


import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;

import com.example.mdiatrkr.R;

public class MediaListView extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>{

    private CursorAdapter cursorAdapter;
    private DBProvider db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_view);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Display existing media
        String[] fromColumns = {DBHelper.MEDIA_FORMAT, DBHelper.MEDIA_CONTENTS, DBHelper.MEDIA_DISC_ORIGIN};
        int [] toViews = {R.id.media_item_format,R.id.media_item_contents, R.id.media_item_origin};

        // Create cursor
        cursorAdapter = new SimpleCursorAdapter(this,R.layout.media_list_item,null, fromColumns, toViews,0);
        db = new DBProvider();

        ListView listView = findViewById(R.id.media_list_view);
        listView.setAdapter(cursorAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long mediaId) {
                Intent intent = new Intent(MediaListView.this, MediaDetailView.class);
                intent.putExtra("mediaId",mediaId);
                startActivity(intent);
            }
        });

        getSupportLoaderManager().initLoader(0,null,this);

        Button addMediaDiscButton = findViewById(R.id.add_media_button);
        addMediaDiscButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaListView.this, DiscInput.class);
                intent.putExtra("discType","Files or Media");
                startActivity(intent);
            }
        });
    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int i, @Nullable Bundle bundle) {
        //Consider modifying order of display
        return new CursorLoader(this, DBProvider.MEDIA_URI, null,null,null,null);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor cursor){
        cursorAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(@NonNull Loader loader) {
        cursorAdapter.swapCursor(null);
    }

    @Override
    public void onResume(){
        super.onResume();
        getSupportLoaderManager().restartLoader(0,null,this);
    }


}