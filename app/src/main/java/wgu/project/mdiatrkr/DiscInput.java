package wgu.project.mdiatrkr;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import com.example.mdiatrkr.R;


public class DiscInput extends AppCompatActivity {

    long discId;
    RadioGroup discTypeGroup;
    RadioGroup discFormatGroup;
    CheckBox rewritableCheckBox;

    Movie movie;
    RadioGroup movieRatingGroup;
    private TextView movieRatingBar;
    private TextView movieDetailsBar;
    private TableLayout movieDetailsTable;
    private EditText movieTitleText;
    private EditText movieDurationText;
    private EditText movieGenreText;

    FileMedia media;
    private TextView mediaDetailsBar;
    RadioGroup mediaOriginGroup;
    private TableLayout mediaInputContentsTable;
    private EditText mediaContentsText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disc_input);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        discId = intent.getLongExtra("discId",-1);

        // Layout setup
        //Common fields, always visible
        discTypeGroup = findViewById(R.id.disc_input_disc_type_radio);
        discFormatGroup = findViewById(R.id.disc_input_format_radio);
        rewritableCheckBox = findViewById(R.id.disc_rewritable_checkbox);

        // movie-only fields, visible when Movie is selected
        movieDetailsBar = findViewById(R.id.movie_details_bar);
        movieDetailsTable = findViewById(R.id.movie_input_details_table);
        movieTitleText = findViewById(R.id.movie_title_entry);
        movieDurationText = findViewById(R.id.movie_duration_entry);
        movieGenreText = findViewById(R.id.movie_input_genre);
        movieRatingBar = findViewById(R.id.movie_rating_bar);
        movieRatingGroup = findViewById(R.id.movie_input_rating_radio);

        //media-only fields, visible when Media is selected
        mediaDetailsBar = findViewById(R.id.media_details_bar);
        mediaInputContentsTable = findViewById(R.id.media_input_contents_table);
        mediaContentsText = findViewById(R.id.media_input_contents);
        mediaOriginGroup = findViewById(R.id.media_input_disc_origin_radio);

        //Save Button
        final Button saveDiscButton = findViewById(R.id.disc_input_save_button);
        saveDiscButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Gather field values common to both disc types
                String discType= ((RadioButton) findViewById((discTypeGroup.getCheckedRadioButtonId()))).getText().toString();
                String discRewritable="No";
                if(rewritableCheckBox.isChecked()){discRewritable="Yes";}
                String discFormat= ((RadioButton) findViewById((discFormatGroup.getCheckedRadioButtonId()))).getText().toString();

                if (discType.equals("Movie")){
                    if (movieTitleText.getText().toString().isEmpty() ||
                            movieDurationText.getText().toString().isEmpty() ||
                            movieGenreText.getText().toString().isEmpty())
                    {
                        Toast.makeText(DiscInput.this,"Empty fields are not allowed.",1).show();
                        return;
                    }
                    String movieTitle = movieTitleText.getText().toString();
                    int movieDuration = 0;
                    //This should be a non-issue as the input for duration only accepts integers...
                    try {
                        movieDuration = Integer.parseInt(movieDurationText.getText().toString());
                    } catch (final NumberFormatException e){
                        Toast.makeText(DiscInput.this,"Please try again with a numerical duration in minutes.",1).show();
                        return;
                    }
                    String movieGenre = movieGenreText.getText().toString();
                    String movieRating = ((RadioButton) findViewById((movieRatingGroup.getCheckedRadioButtonId()))).getText().toString();
                    ContentValues cv = new ContentValues();
                    cv.put(DBHelper.MOVIES_TITLE,movieTitle);
                    cv.put(DBHelper.MOVIES_DURATION,movieDuration);
                    cv.put(DBHelper.MOVIES_GENRE,movieGenre);
                    cv.put(DBHelper.MOVIES_RATING,movieRating);
                    cv.put(DBHelper.MOVIES_REWRITABLE,discRewritable);
                    cv.put(DBHelper.MOVIES_FORMAT,discFormat);
                    if (discId==-1){
                        addDisc(discType, cv);
                    } else updateDisc(discType,discId, cv);
                    finish();
                } else{
                    if (mediaContentsText.getText().toString().isEmpty())
                    {
                        Toast.makeText(DiscInput.this,"Please describe the contents of the disc.",1).show();
                        return;
                    }
                    ContentValues cv = new ContentValues();
                    String mediaType = ((RadioButton) findViewById((mediaOriginGroup.getCheckedRadioButtonId()))).getText().toString();
                    String mediaContents = mediaContentsText.getText().toString();
//                    mediaOriginGroup.setVisibility(View.GONE);
                    cv.put(DBHelper.MEDIA_DISC_ORIGIN,mediaType);
                    cv.put(DBHelper.MEDIA_CONTENTS,mediaContents);
                    cv.put(DBHelper.MEDIA_REWRITABLE,discRewritable);
                    cv.put(DBHelper.MEDIA_FORMAT,discFormat);
                    if (discId==-1){
                        addDisc(discType, cv);
                    } else updateDisc(discType,discId, cv);
                    finish();
                }
            }
        });

        String passedDiscType = intent.getStringExtra("discType");
        switch (passedDiscType){
            case "Movie":               //set Media entry fields to GONE
                RadioButton tempMovieButton = (RadioButton) (findViewById(R.id.disc_input_movie_radio));
                tempMovieButton.setChecked(true);
                mediaDetailsBar.setVisibility(View.GONE);
                mediaInputContentsTable.setVisibility(View.GONE);
                mediaOriginGroup.setVisibility(View.GONE);
                movieDetailsBar.setVisibility(View.VISIBLE);
                movieDetailsTable.setVisibility(View.VISIBLE);
                movieRatingBar.setVisibility(View.VISIBLE);
                movieRatingGroup.setVisibility(View.VISIBLE);
                saveDiscButton.setVisibility(View.VISIBLE);
                break;
            case "Files or Media":      //set Movie entry fields to GONE
                RadioButton tempMediaButton = (RadioButton) (findViewById(R.id.disc_input_media_radio));
                tempMediaButton.setChecked(true);
                mediaDetailsBar.setVisibility(View.VISIBLE);
                mediaInputContentsTable.setVisibility(View.VISIBLE);
                mediaOriginGroup.setVisibility(View.VISIBLE);
                movieDetailsBar.setVisibility(View.GONE);
                movieDetailsTable.setVisibility(View.GONE);
                movieRatingBar.setVisibility(View.GONE);
                movieRatingGroup.setVisibility(View.GONE);
                saveDiscButton.setVisibility(View.VISIBLE);
                break;
            default:                    /*set Save button and all type-specific fields to GONE; should only be accessible by coming from the
                                         main activity; navigating here from the Movie/Media list or details should trigger the other options*/
                mediaDetailsBar.setVisibility(View.GONE);
                mediaInputContentsTable.setVisibility(View.GONE);
                mediaOriginGroup.setVisibility(View.GONE);
                movieDetailsBar.setVisibility(View.GONE);
                movieDetailsTable.setVisibility(View.GONE);
                movieRatingBar.setVisibility(View.GONE);
                movieRatingGroup.setVisibility(View.GONE);
                saveDiscButton.setVisibility(View.GONE);
        }

        discTypeGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                String selectedRadioButtonText=((RadioButton) findViewById((discTypeGroup.getCheckedRadioButtonId()))).getText().toString();
                switch (selectedRadioButtonText){
                    case "Movie":               //set media entry fields to invisible
                        mediaDetailsBar.setVisibility(View.GONE);
                        mediaInputContentsTable.setVisibility(View.GONE);
                        mediaOriginGroup.setVisibility(View.GONE);
                        movieDetailsBar.setVisibility(View.VISIBLE);
                        movieDetailsTable.setVisibility(View.VISIBLE);
                        movieRatingBar.setVisibility(View.VISIBLE);
                        movieRatingGroup.setVisibility(View.VISIBLE);
                        saveDiscButton.setVisibility(View.VISIBLE);
                        break;
                    case "Files or Media":      //set movie entry fields to invisible
                        mediaDetailsBar.setVisibility(View.VISIBLE);
                        mediaInputContentsTable.setVisibility(View.VISIBLE);
                        mediaOriginGroup.setVisibility(View.VISIBLE);
                        movieDetailsBar.setVisibility(View.GONE);
                        movieDetailsTable.setVisibility(View.GONE);
                        movieRatingBar.setVisibility(View.GONE);
                        movieRatingGroup.setVisibility(View.GONE);
                        saveDiscButton.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });

        if (discId!=-1) {
            //fetch and set type-specific values
            switch (passedDiscType) {
                case "Movie":
                    //fetch existing movie
                    Cursor tempMovieCursor = getContentResolver().query(DBProvider.MOVIES_URI, null, DBHelper.MOVIES_ID + " = " + discId, null, null);
                    if (tempMovieCursor.moveToFirst()) {
                        movie = MovieDetailView.convertCursorToMovie(tempMovieCursor);
                    }
                    //fill in values from db
                    movieTitleText.setText(movie.getTitle());
                    movieDurationText.setText(String.valueOf(movie.getDuration()));
                    movieGenreText.setText(movie.getGenre());
                    switch (movie.getRating()){
                    case "R":
                        RadioButton tempRButton = (RadioButton) (findViewById(R.id.movie_input_rating_r_radio));
                        tempRButton.setChecked(true);
                        break;
                    case "PG-13":
                        RadioButton tempPG13Button = (RadioButton) (findViewById(R.id.movie_input_rating_pg13_radio));
                        tempPG13Button.setChecked(true);
                        break;
                    case "PG":
                        RadioButton tempPGButton = (RadioButton) (findViewById(R.id.movie_input_rating_pg_radio));
                        tempPGButton.setChecked(true);
                        break;
                    case "G":
                        RadioButton tempGButton = (RadioButton) (findViewById(R.id.movie_input_rating_g_radio));
                        tempGButton.setChecked(true);
                        break;
                }
                    switch (movie.getFormat()){
                        case "BluRay":
                            RadioButton tempBRButton = (RadioButton) (findViewById(R.id.disc_input_format_BR_radio));
                            tempBRButton.setChecked(true);
                            break;
                        case "DVD":
                            RadioButton tempDVDButton = (RadioButton) (findViewById(R.id.disc_input_format_DVD_radio));
                            tempDVDButton.setChecked(true);
                            break;
                        case "CD":
                            RadioButton tempCDButton = (RadioButton) (findViewById(R.id.disc_input_format_CD_radio));
                            tempCDButton.setChecked(true);
                            break;
                    }
                    if (movie.getRewritable().equals("Yes")){rewritableCheckBox.setChecked(true);}
                        else {rewritableCheckBox.setChecked(false);}

                    break;
                case "Files or Media":
                    Cursor tempMediaCursor = getContentResolver().query(DBProvider.MEDIA_URI, null, DBHelper.MEDIA_ID + " = " + discId, null, null);
                    if (tempMediaCursor.moveToFirst()) {
                        media = MediaDetailView.convertCursorToFileMedia(tempMediaCursor);
                    }
                    mediaContentsText.setText(media.getContents());
                    switch (media.getDiscOrigin()){
                        case "Homemade":
                            RadioButton tempHomemadeButton = (RadioButton) (findViewById(R.id.media_input_homemade_radio));
                            tempHomemadeButton.setChecked(true);
                            break;
                        case "Retail":
                            RadioButton tempRetailButton = (RadioButton) (findViewById(R.id.media_input_retail_radio));
                            tempRetailButton.setChecked(true);
                            break;
                    }
                    switch (media.getFormat()) {
                        case "BluRay":
                            RadioButton tempBRButton = (RadioButton) (findViewById(R.id.disc_input_format_BR_radio));
                            tempBRButton.setChecked(true);
                            break;
                        case "DVD":
                            RadioButton tempDVDButton = (RadioButton) (findViewById(R.id.disc_input_format_DVD_radio));
                            tempDVDButton.setChecked(true);
                            break;
                        case "CD":
                            RadioButton tempCDButton = (RadioButton) (findViewById(R.id.disc_input_format_CD_radio));
                            tempCDButton.setChecked(true);
                            break;
                    }
                    if (media.getRewritable().equals("Yes")){rewritableCheckBox.setChecked(true);}
                        else {rewritableCheckBox.setChecked(false);}
                    break;
                    }
            }
        }

    private void addDisc(String discType, ContentValues cv){
        switch (discType){
            case "Movie":
                Uri movieUri = getContentResolver().insert(DBProvider.MOVIES_URI,cv);
                return;
            case "Files or Media":
                Uri mediaUri = getContentResolver().insert(DBProvider.MEDIA_URI,cv);
                return;
        }
    }

    private void updateDisc(String discType, long discId, ContentValues cv){
        switch (discType){
            case "Movie":
                int updateMovie = getContentResolver().update(DBProvider.MOVIES_URI,cv,DBHelper.MOVIES_ID+" = "+discId,null);
                return;
            case "Files or Media":
                int updateMedia = getContentResolver().update(DBProvider.MEDIA_URI,cv,DBHelper.MEDIA_ID+" = "+discId,null);
                return;
        }
    }
}