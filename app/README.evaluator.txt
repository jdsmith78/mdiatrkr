Here's a quick reference to easily locate the items required by the rubric in this application.

•   one of the following application types: mobile, web, or stand-alone application
    This is obviously a mobile application.

•   code including inheritance, polymorphism, and encapsulation
    Note that the FileMedia and Movie classes are children of the Disc class, with a few inherited attributes and a few unique ones.

•   search functionality with multiple row results and displays
    Using the search function under Search Discs will demonstrate this.

•   a database component with the functionality to securely add, modify, and delete the data
    See the DBHelper and DBProvider classes for the inner workings of the SQLite database in use here. The DiscInput class handles the heavy
    lifting for add/update, and delete is an option in the MovieDetailView and MediaDetailView activities.

•   ability to generate reports with multiple columns, multiple rows, date-time stamp, and title
    See the two column report in the DiscListReport activity.

•   exception controls
    The only sensitive data type used is Duration, which is an int. The blank the user has to supply it only takes integers, but as a failsafe
    the DiscInput activity checks the input and throws an exception if the input is not an int.

•   validation functionality
    The DiscInput activity checks for empty fields before allowing the user to save the disc to the database. In a similar vein, the Save button
    is invisible until the user selects a disc type (Movie for Files/Media). Several fields use radio or checkboxes for input to avoid the need
    for validation and smooth the user experience.

•   industry appropriate security features
    The Android manifest is configured such that the only application that can launch any of the activities in the app is the MainActivity. The
    content provider is marked as not exported to prevent its use by any other application.

•   design elements that make the application scalable
    The various list windows scroll when their contents are too numerous to fit on the screen. The easiest tests for this would be to add a Media
    disc and then check the MediaListView activity (via the View File Media button from the main activity) or to look at the Disc List Report
    with the test data.

•   a user-friendly, functional GUI
    It's difficult to point to a single spot for this, but note the use of radiobuttons where a limited set of values are expected (rating,
    format, retail/homemade, etc.) and a checkbox for whether a disc is rewritable or not.